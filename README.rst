tower-cli
=========

Docker_ image to run `tower-cli <http://tower-cli.readthedocs.io/en/latest/>`_, a command line tool for Ansible Tower and AWX_.

The image also includes xmllint and a simple script `get_pom_version` to get the version from the current pom.xml file.

.. _Docker: https://www.docker.com
.. _AWX: https://github.com/ansible/awx
