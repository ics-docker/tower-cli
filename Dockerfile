FROM python:3.6-alpine

LABEL maintainer "benjamin.bertrand@esss.se"

RUN adduser -S csi

ENV TOWER_CLI_VERSION 3.3.0

RUN apk add --no-cache libxml2-utils
RUN pip install --no-cache-dir ansible-tower-cli=="$TOWER_CLI_VERSION"

COPY get_pom_version /usr/bin

USER csi
